
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
// var AccountModule        = require("../lib/module/Account");
// var ApiAccountModule     = require("../lib/module/ApiAccount");
var BoardModule          = require("../lib/module/Board");
// var CommentModule        = require("../lib/module/Comment");
var LaneModule           = require("../lib/module/Lane");
var ProjectModule        = require("../lib/module/Project");
// var SprintModule         = require("../lib/module/Sprint");
var TagModule            = require("../lib/module/Tag");
// var TicketActivityModule = require("../lib/module/TicketActivity");
// var TicketLinkModule     = require("../lib/module/TicketLink");
// var TicketStatusModule   = require("../lib/module/TicketStatus");
// var TicketModule         = require("../lib/module/Ticket");
// var TimeLogModule        = require("../lib/module/TimeLog");
var UserModule           = require("../lib/module/User");

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }

  res.redirect("/login");
}

function ensureAuthenticatedApi(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }

  return res.status(403).json("Forbidden");
}

module.exports = function(router, passport) {

  // // account resources
  // router.get("/api/accounts", ensureAuthenticatedApi, AccountModule.Routes.Api.get);
  // router.get("/api/accounts/:id", ensureAuthenticatedApi, AccountModule.Routes.Api.getById);
  // router.post("/api/accounts", ensureAuthenticatedApi, AccountModule.Routes.Api.create);
  // router.put("/api/accounts/:id", ensureAuthenticatedApi, AccountModule.Routes.Api.update);

  // // api account resources
  // router.get("/api/api-accounts", ensureAuthenticatedApi, ApiAccountModule.Routes.Api.get);
  // router.get("/api/api-accounts/:id", ensureAuthenticatedApi, ApiAccountModule.Routes.Api.getById);
  // router.post("/api/api-accounts", ensureAuthenticatedApi, ApiAccountModule.Routes.Api.create);
  // router.put("/api/api-accounts/:id", ensureAuthenticatedApi, ApiAccountModule.Routes.Api.update);

  // board resources
  router.get("/api/projects/:projectId/boards", ensureAuthenticatedApi, BoardModule.Routes.BoardRoutes.get);
  router.get("/api/projects/:projectId/boards/:boardId", ensureAuthenticatedApi, BoardModule.Routes.BoardRoutes.getById);
  router.post("/api/projects/:projectId/boards", ensureAuthenticatedApi, BoardModule.Routes.BoardRoutes.create);
  router.put("/api/projects/:projectId/boards/:boardId", ensureAuthenticatedApi, BoardModule.Routes.BoardRoutes.update);

  // // comment resources
  // router.get("/api/comments", ensureAuthenticated, CommentModule.Routes.Api.get);
  // router.get("/api/comments/:id", ensureAuthenticatedApi, CommentModule.Routes.Api.getById);
  // router.post("/api/comments", ensureAuthenticatedApi, CommentModule.Routes.Api.create);
  // router.put("/api/comments/:id", ensureAuthenticatedApi, CommentModule.Routes.Api.update);

  // lane resources
  router.get("/api/projects/:projectId/boards/:boardId/lanes", ensureAuthenticated, LaneModule.Routes.LaneRoutes.get);
  router.get("/api/projects/:projectId/boards/:boardId/lanes/:laneId", ensureAuthenticatedApi, LaneModule.Routes.LaneRoutes.getById);
  router.post("/api/projects/:projectId/boards/:boardId/lanes", ensureAuthenticatedApi, LaneModule.Routes.LaneRoutes.create);
  router.put("/api/projects/:projectId/boards/:boardId/lanes/:laneId", ensureAuthenticatedApi, LaneModule.Routes.LaneRoutes.update);

  // project resources
  router.get("/api/projects", ensureAuthenticated, ProjectModule.Routes.ProjectRoutes.get);
  router.get("/api/projects/:projectId", ensureAuthenticatedApi, ProjectModule.Routes.ProjectRoutes.getById);
  router.post("/api/projects", ensureAuthenticatedApi, ProjectModule.Routes.ProjectRoutes.create);
  router.put("/api/projects/:projectId", ensureAuthenticatedApi, ProjectModule.Routes.ProjectRoutes.update);

  // // sprint resources
  // router.get("/api/sprints", ensureAuthenticated, SprintModule.Routes.Api.get);
  // router.get("/api/sprints/:id", ensureAuthenticatedApi, SprintModule.Routes.Api.getById);
  // router.post("/api/sprints", ensureAuthenticatedApi, SprintModule.Routes.Api.create);
  // router.put("/api/sprints/:id", ensureAuthenticatedApi, SprintModule.Routes.Api.update);

  // tag resources
  router.get("/api/tags", ensureAuthenticated, TagModule.Routes.TagRoutes.get);
  router.get("/api/tags/:tagId", ensureAuthenticatedApi, TagModule.Routes.TagRoutes.getById);
  router.post("/api/tags", ensureAuthenticatedApi, TagModule.Routes.TagRoutes.create);
  router.put("/api/tags/:tagId", ensureAuthenticatedApi, TagModule.Routes.TagRoutes.update);

  // // ticket activity resources
  // router.get("/api/ticket-activities", ensureAuthenticated, TicketActivityModule.Routes.Api.get);
  // router.get("/api/ticket-activities/:id", ensureAuthenticatedApi, TicketActivityModule.Routes.Api.getById);
  // router.post("/api/ticket-activities", ensureAuthenticatedApi, TicketActivityModule.Routes.Api.create);
  // router.put("/api/ticket-activities/:id", ensureAuthenticatedApi, TicketActivityModule.Routes.Api.update);

  // // ticket links resources
  // router.get("/api/ticket-links", ensureAuthenticated, TicketLinkModule.Routes.Api.get);
  // router.get("/api/ticket-links/:id", ensureAuthenticatedApi, TicketLinkModule.Routes.Api.getById);
  // router.post("/api/ticket-links", ensureAuthenticatedApi, TicketLinkModule.Routes.Api.create);
  // router.put("/api/ticket-links/:id", ensureAuthenticatedApi, TicketLinkModule.Routes.Api.update);

  // // ticket status resources
  // router.get("/api/ticket-statuses", ensureAuthenticated, TicketStatusModule.Routes.Api.get);
  // router.get("/api/ticket-statuses/:id", ensureAuthenticatedApi, TicketStatusModule.Routes.Api.getById);
  // router.post("/api/ticket-statuses", ensureAuthenticatedApi, TicketStatusModule.Routes.Api.create);
  // router.put("/api/ticket-statuses/:id", ensureAuthenticatedApi, TicketStatusModule.Routes.Api.update);

  // // ticket resources
  // router.get("/api/tickets", ensureAuthenticated, TicketModule.Routes.Api.get);
  // router.get("/api/tickets/:id", ensureAuthenticatedApi, TicketModule.Routes.Api.getById);
  // router.post("/api/tickets", ensureAuthenticatedApi, TicketModule.Routes.Api.create);
  // router.put("/api/tickets/:id", ensureAuthenticatedApi, TicketModule.Routes.Api.update);

  // // time log resources
  // router.get("/api/time-logs", ensureAuthenticated, TimeLogModule.Routes.Api.get);
  // router.get("/api/time-logs/:id", ensureAuthenticatedApi, TimeLogModule.Routes.Api.getById);
  // router.post("/api/time-logs", ensureAuthenticatedApi, TimeLogModule.Routes.Api.create);
  // router.put("/api/time-logs/:id", ensureAuthenticatedApi, TimeLogModule.Routes.Api.update);

  // user resources
  router.get("/api/users", ensureAuthenticated, UserModule.Routes.ApiRoutes.get);
  router.get("/api/users/:userId", ensureAuthenticatedApi, UserModule.Routes.ApiRoutes.getById);
  router.post("/api/users", ensureAuthenticatedApi, UserModule.Routes.ApiRoutes.create);
  router.put("/api/users/:userId", ensureAuthenticatedApi, UserModule.Routes.ApiRoutes.update);

  router.post("/api/users/avatars", ensureAuthenticated, UserModule.Routes.FrontendRoutes.uploadAvatar);

  // ------------------------------------------------------------------------------
  // -------------------------------- FRONTEND URLS -------------------------------
  // ------------------------------------------------------------------------------

  router.get("/user", ensureAuthenticated, UserModule.Routes.FrontendRoutes.getDetails);

  router.get("/login", function(req, res){
    res.status(200).sendFile(appRootPath + "/public/login.html");
  });

  router.post("/login",
    passport.authenticate("local"),
    function(req, res) {
      res.status(200).json("OK");
    }
  );

  router.get("/logout", function(req, res) {
    req.logout();
    res.redirect("/login");
  });

  // load the single view file
  // (angular will handle the page changes on the front-end)
  router.get("*", ensureAuthenticated, function(req, res) {
    res.sendFile(appRootPath + "/public/index.html");
  });
};
