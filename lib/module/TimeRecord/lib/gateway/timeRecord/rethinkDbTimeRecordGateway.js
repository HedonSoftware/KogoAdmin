
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

var MysqlMapper  = require(appRootPath + "/lib/mapper/rethinkdb");
var logger          = require(appRootPath + "/lib/logger/logger");
var TimeLog      = require("./../entity/timeLog");
var User         = require(appRootPath + "/lib/module/User/lib/entity/user");
var Ticket       = require(appRootPath + "/lib/module/Ticket/lib/entity/ticket");
var util         = require("util");
var Exception    = require(appRootPath + "/lib/exception");
var _            = require(appRootPath + "/lib/utility/underscore");
var Request = require(appRootPath + "/lib/request/request");

/**
 * TimeLogs mapper class definition
 * @param Connection dbConnection Optional DI for connection
 */
function TimeLogsMapper(dbConnection) {
  dbConnection = dbConnection || require(appRootPath + "/lib/db/mysql");
  this.setDbConnection(dbConnection);
};

/**
 * TimeLogsMapper extends MysqlMapper
 */
util.inherits(TimeLogsMapper, MysqlMapper);

/**
 * Table info
 * @type object
 */
TimeLogsMapper.prototype.table = {
  "name": "TimeLog",
  "alias": "tl"
};

/**
 * Object describing timeLogs"s relation
 *
 * @type Object
 */
TimeLogsMapper.prototype.relations = {
  "user": {
    "table": "User",
    "localColumn": "userId",
    "targetColumn": "id",
    "defaultAlias": "u",
    "condition": "`tl`.`userId` = `u`.`id`",
    "query": "`User` AS `u` ON `tl`.`userId` = `u`.`id`"
  },
  "ticket": {
    "table": "Ticket",
    "localColumn": "ticketId",
    "targetColumn": "id",
    "defaultAlias": "t",
    "condition": "`tl`.`ticketId` = `t`.`id`",
    "query": "`Ticket` AS `t` ON `tl`.`ticketId` = `t`.`id`"
  }
}

/**
 * Method fetches all records matching passed query builder"s criteria
 *
 * @param  Request request Used to specify the query
 * @return Promise      promise      Promise of DB result
 */
TimeLogsMapper.prototype.fetchAll = function (request) {

  var timeLog = new TimeLog();
  var entities  = [timeLog];

  var joins = request.getJoins();
  if (_.contains(joins, "user")) {
    entities.push(new User());
  }
  if (_.contains(joins, "ticket")) {
    entities.push(new Ticket());
  }

  var queryData = this.generateQuery(request, entities).toParam();
  return this.query(queryData.text, queryData.values);
};

TimeLogsMapper.prototype.save = function (timeLog) {

  if (!(timeLog instanceof TimeLog)) {
    throw new Exception("Invalid entity passed. Instance of TimeLog expected");
  }

  var me = this;
  var request = new Request();

  if (!timeLog.getId()) {
    return this.insert(timeLog);
  }

  request.setConditions({id: timeLog.getId()});
  return this.fetchOne(request)
    .then(function (timeLogFromDb) {

      if (!timeLogFromDb) {
        return me.insert(timeLog);
      }

      var baseTimeLog = new TimeLog(timeLogFromDb);

      // delete properties if not set
      if (!timeLog.createdAt) {
        delete timeLog.createdAt;
        timeLog.createdAt = baseTimeLog.createdAt;
      }
      if (!timeLog.updatedAt) {
        delete timeLog.updatedAt;
      }

      return me.update(timeLog);
    });
};

TimeLogsMapper.prototype.insert = function (timeLog) {

  if (!(timeLog instanceof TimeLog)) {
    throw new Exception("Invalid entity passed. Instance of TimeLog expected");
  }

  var me = this;
  var queryData = this.generateInsertQuery(timeLog).toParam();
  return this.query(queryData.text, queryData.values)
    .then(function (data) {
      var details = data.shift();
      var request = new Request();
      request.setConditions({id: details.insertId});
      return me.fetchOne(request);
    })
    .then(function (timeLogData) {
      return new TimeLog(timeLogData);
    });
};

TimeLogsMapper.prototype.update = function (timeLog) {

  if (!(timeLog instanceof TimeLog)) {
    throw new Exception("Invalid entity passed. Instance of TimeLog expected");
  }

  if (!timeLog.getId()) {
    throw new Exception("Invalid logic. Insert should be called");
  }

  var me = this;
  var queryData = this.generateUpdateQuery(timeLog).toParam();
  return this.query(queryData.text, queryData.values)
    .then(function (data) {
      var request = new Request();
      request.setConditions({id: timeLog.getId()});
      return me.fetchOne(request);
    })
    .then(function (timeLogData) {
      return new TimeLog(timeLogData);
    });
};

TimeLogsMapper.prototype.delete = function (id) {

  var queryData = this.generateDeleteQuery({"id":id}).toParam();
  return this.query(queryData.text, queryData.values)
    .then(function (result) {
      var details = result.shift();
      return details.affectedRows;
    });
};

module.exports = TimeLogsMapper;
