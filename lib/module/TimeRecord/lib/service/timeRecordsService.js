
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

var TimeLogEntity = require("../entity/timeLog");
var Request      = require(appRootPath + "/lib/request/request");
var _            = require(appRootPath + "/lib/utility/underscore");
var logger          = require(appRootPath + "/lib/logger/logger");

/**
 * TimeLogs service class definition
 */
function TimeLogs(timeLogsMapper) {
  if (!timeLogsMapper) {
    var TimeLogsMapper = require("../mappers/timeLogs");
    var timeLogsMapper = new TimeLogsMapper;
  }

  this.timeLogsMapper = timeLogsMapper;
}

/**
 * Method gets all timeLogs matching passed query
 * @param  {[type]} query   [description]
 * @return Promise  promise [description]
 */
TimeLogs.prototype.all = function (query) {
  query = query || new Request();
  return this.timeLogsMapper.fetchAll(query)
    .then(function (data) {
      return data.shift() || [];
    });
};

/**
 * Method gets single timeLog by passed id
 * @param  {[type]}   id       [description]
 * @return {[type]}            [description]
 */
TimeLogs.prototype.get = function (id) {

  var query = new Request();
  query.setConditions({"id": id});

  return this.all(query)
    .then(function (timeLogs) {
      if (!_.isArray(timeLogs) || _.isEmpty(timeLogs)) {
        return null;
      }
      return timeLogs.shift();
    });
};

TimeLogs.prototype.save = function (data) {
  var timeLog = new TimeLogEntity(data);
  return this.timeLogsMapper.save(timeLog);
};

TimeLogs.prototype.del = function (id) {
  return this.timeLogsMapper.delete(id);
};

module.exports = TimeLogs;
