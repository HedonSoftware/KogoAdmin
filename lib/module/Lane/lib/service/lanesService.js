
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var LaneEntity  = require("../entity/laneEntity");
var Request     = require(appRootPath + "/lib/request/request");
var _           = require(appRootPath + "/lib/utility/underscore");
var TagEntity   = require(appRootPath + "/lib/module/Tag/lib/entity/tagEntity");

class LanesService
{
  constructor(laneGateway, projectGateway, boardGateway, tagGateway)
  {
    if (!laneGateway) {
      var LaneGateway = require(appRootPath + "/lib/module/Lane/lib/gateway/lane/httpLaneGateway");
      laneGateway = new LaneGateway();
    }

    if (!projectGateway) {
      var ProjectGateway = require(appRootPath + "/lib/module/Project/lib/gateway/project/httpProjectGateway");
      projectGateway = new ProjectGateway();
    }

    if (!boardGateway) {
      var BoardGateway = require(appRootPath + "/lib/module/Board/lib/gateway/board/httpBoardGateway");
      boardGateway = new BoardGateway();
    }

    if (!tagGateway) {
      var TagGateway = require(appRootPath + "/lib/module/Tag/lib/gateway/tag/httpTagGateway");
      tagGateway = new TagGateway();
    }

    this.laneGateway    = laneGateway;
    this.projectGateway = projectGateway;
    this.boardGateway   = boardGateway;
    this.tagGateway     = tagGateway;
  }

  /**
   * Method gets all lanes matching passed query
   * @param  {[type]}   query    [description]
   * @return {[type]}            [description]
   */
  all(projectId, boardId, query)
  {
    query = query || new Request();
    return this.laneGateway.fetchAll(projectId, boardId, query)
      .then(function (data) {
        return data || [];
      });
  }

  /**
   * Method gets single lane by passed id
   * @param  {[type]}   id       [description]
   * @return {[type]}            [description]
   */
  fetchOne(projectId, boardId, query)
  {
    return this.all(projectId, boardId, query)
      .then(function (lanes) {
        if (!_.isArray(lanes) || _.isEmpty(lanes)) {
          return null;
        }
        return lanes.shift();
      });
  }

  create(projectId, boardId, data)
  {
    var me = this;
    var lane = new LaneEntity(data);
    var project, board;

    var projectRequest = new Request({
      conditions: {
        id: projectId
      },
      fields: ["name"]
    });
    return this.projectGateway.fetchAll(projectRequest)
      .then(function(results) {

        project = results.pop();

        var boardRequest = new Request({
          conditions: {
            id: boardId
          },
          fields: ["name"]
        });

        return me.boardGateway.fetchAll(projectId, boardRequest);

      }).then(function(results) {

        board = results.pop();

        var tagEntity = new TagEntity({
          "name": project.name + ":" + board.name + ":" + lane.get("name"),
          "description": "Tag for " + lane.get("name") + " lane",
          "projectId": projectId,
          "boardId": boardId,
          "type": "lane",
          "status": "active"
        });

        return me.tagGateway.insert(tagEntity);

      }).then(function(tag) {

        lane.tags = [tag.get("id")];

        return me.laneGateway.insert(projectId, boardId, lane);

      });
  }

  update(projectId, boardId, data)
  {
    var lane = new LaneEntity(data);
    return this.laneGateway.update(projectId, boardId, lane);
  }

  replace(projectId, boardId, data)
  {
    var lane = new LaneEntity(data);
    return this.laneGateway.replace(projectId, boardId, lane);
  }

  delete(projectId, boardId, laneId)
  {
    return this.laneGateway.delete(projectId, boardId, laneId);
  }
}

module.exports = LanesService;
