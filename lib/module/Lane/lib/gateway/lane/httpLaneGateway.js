
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var HttpGateway  = require(appRootPath + "/lib/gateway/httpGateway");
var LaneEntity   = require(appRootPath + "/lib/module/Lane/lib/entity/laneEntity");
var _            = require(appRootPath + "/lib/utility/underscore");
var GatewayError = require(appRootPath + "/lib/gateway/gatewayError");
var Request      = require(appRootPath + "/lib/request/request");
var logger       = require(appRootPath + "/lib/logger/logger");

class HttpLaneGateway extends HttpGateway
{
  /**
   * Cutom contractor allows to pass data provider instance
   *
   * @param object httpDataProvider DataProvider's instance(i.e. httpDataProvider)
   */
  constructor(httpDataProvider)
  {
    super(httpDataProvider);
    this.dataProvider.apiUrlParts = [
      "/projects/",
      null,
      "/boards/",
      null,
      "/lanes"
    ];
  }

  /**
   * Method fetches all records matching passed request criteria
   *
   * @param  Request request Used to specify the query
   * @return Promise promise Promise of data from DB
   */
  fetchAll(projectId, boardId, request)
  {
    logger.info("HttpLaneGateway::fetchAll() method called", {
      projectId: projectId,
      boardId: boardId,
      request: request.export()
    });

    this.setupUrl(projectId, boardId);
    return this.dataProvider.sendGetRequest(request);
  }

  save(projectId, boardId, laneEntity)
  {
    logger.info("HttpLaneGateway::save() method called", {
      projectId: projectId,
      boardId: boardId,
      laneEntity: laneEntity
    });

    this.setupUrl(projectId, boardId);
    return super.save(laneEntity);
  }

  insert(projectId, boardId, laneEntity)
  {
    logger.info("HttpLaneGateway::insert() method called", {
      projectId: projectId,
      boardId: boardId,
      laneEntity: laneEntity
    });

    this.setupUrl(projectId, boardId);

    if (!(laneEntity instanceof LaneEntity)) {
      throw new GatewayError("Invalid model passed. Instance of LaneEntity expected");
    }

    return super.insert(laneEntity)
      .then(function(laneData) {
        return new LaneEntity(laneData);
      });
  }

  update(projectId, boardId, laneEntity)
  {
    logger.info("HttpLaneGateway::update() method called", {
      projectId: projectId,
      boardId: boardId,
      laneEntity: laneEntity
    });

    this.setupUrl(projectId, boardId);

    if (!(laneEntity instanceof LaneEntity)) {
      throw new GatewayError("Invalid model passed. Instance of Board expected");
    }

    if (!laneEntity.getId()) {
      throw new GatewayError("Invalid logic. Insert should be called");
    }

    return super.update(laneEntity)
      .then(function(boardData) {
        return new LaneEntity(boardData);
      });
  }

  setupUrl(projectId, boardId)
  {
    logger.info("HttpLaneGateway::setupUrl() method called", {
      projectId: projectId,
      boardId: boardId
    });

    this.dataProvider.apiUrlParts[1] = projectId;
    this.dataProvider.apiUrlParts[3] = boardId;
    this.dataProvider.apiUrl = this.dataProvider.apiUrlParts.join("");
  }
}

module.exports = HttpLaneGateway;
