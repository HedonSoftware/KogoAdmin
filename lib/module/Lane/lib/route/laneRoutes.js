
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var LanesService = require("../service/lanesService");
var logger       = require(appRootPath + "/lib/logger/logger");
var errorHandler = require(appRootPath + "/lib/error/errorHandler");

module.exports.get = function (req, res) {
  logger.info("LaneRoutes::get - request received", {
    httpMethod: req.method,
    url: req.url,
    query: req.query.export()
  });

  var lanesService = new LanesService();
  try {
    lanesService.all(req.params.projectId, req.params.boardId, req.query)
      .then(function (lanes) {
        logger.info("LaneRoutes::get - 200 response sent back", {
          lanes: lanes
        });
        return res.status(200).json(lanes);
      })
      .catch(function (error) {
        return errorHandler.resolve(error, req, res);
      });
  } catch (error) {
    return errorHandler.resolve(error, req, res);
  }
};

module.exports.getById = function (req, res) {
  logger.info("LaneRoutes::getById - request received", {
    httpMethod: req.method,
    url: req.url,
    query: req.query.export()
  });

  var queryConditions = req.query.getConditions();
  queryConditions.id = req.params.laneId;
  req.query.setConditions(queryConditions);

  var lanesService = new LanesService();
  try {
    lanesService.fetchOne(req.params.projectId, req.params.boardId, req.query)
      .then(function (lane) {
        if (!lane) {
          logger.info("LaneRoutes::getId - 404 response sent back");
          return res.status(404).json("Not Found");
        }
        logger.info("LaneRoutes::getId - 200 response sent back", {
          lane: lane
        });
        return res.status(200).json(lane);
      })
      .catch(function (error) {
        return errorHandler.resolve(error, req, res);
      });
  } catch (error) {
    return errorHandler.resolve(error, req, res);
  }
};

module.exports.create = function (req, res) {
  logger.info("LaneRoutes::create - request received", {
    httpMethod: req.method,
    url: req.url,
    body: req.body,
    params: req.params
  });

  var lanesService = new LanesService();
  try {
    lanesService.create(req.params.projectId, req.params.boardId, req.body)
      .then(function (lane) {
        if (lane === null) {
          logger.info("LaneRoutes::create - 404 response sent back");
          return res.status(404).json("Not Found");
        }

        var locationHeader = "/projects/" + req.params.projectId + "/boards/" +
                             req.params.boardId + "/lanes/" +  lane.getId();

        res.setHeader("Location", locationHeader);
        logger.info("LaneRoutes::create - 201 response sent back", {
          locationHeader: locationHeader,
          lane: lane
        });
        return res.status(201).json(lane.export());
      })
      .catch(function (error) {
        return errorHandler.resolve(error, req, res);
      });
  } catch (error) {
    return errorHandler.resolve(error, req, res);
  }
};

module.exports.update = function (req, res) {
  logger.info("LaneRoutes::update - request received", {
    httpMethod: req.method,
    url: req.url,
    body: req.body,
    params: req.params
  });

  req.body.id = req.params.laneId;

  var lanesService = new LanesService();
  try {
    lanesService.update(req.params.projectId, req.params.boardId, req.body)
      .then(function (lane) {
        if (lane === null) {
          logger.info("LaneRoutes::update - 409 response sent back");
          return res.status(409).json("Conflict");
        }
        logger.info("LaneRoutes::update - 200 response sent back", {
          lane: lane
        });
        return res.status(200).json(lane.export());
      })
      .catch(function (error) {
        return errorHandler.resolve(error, req, res);
      });
  } catch (error) {
    return errorHandler.resolve(error, req, res);
  }
};

module.exports.replace = function (req, res) {
  logger.info("LaneRoutes::replace - request received", {
    httpMethod: req.method,
    url: req.url,
    body: req.body,
    params: req.params
  });

  req.body.id = req.params.laneId;

  var lanesService = new LanesService();
  try {
    lanesService.replace(req.params.projectId, req.params.boardId, req.body)
      .then(function (lane) {
        if (lane === null) {
          logger.info("LaneRoutes::replace - 409 response sent back");
          return res.status(409).json("Conflict");
        }
        logger.info("LaneRoutes::replace - 200 response sent back", {
          lane: lane
        });
        return res.status(200).json(lane.export());
      })
      .catch(function (error) {
        return errorHandler.resolve(error, req, res);
      });
  } catch (error) {
    return errorHandler.resolve(error, req, res);
  }
};

module.exports.delete = function (req, res) {
  logger.info("LaneRoutes::delete - request received", {
    httpMethod: req.method,
    url: req.url,
    body: req.body,
    params: req.params
  });

  var lanesService = new LanesService();
  try{
    lanesService.delete(req.params.projectId, req.params.boardId, req.params.laneId)
      .then(function (numberOfDeleted) {
        if (numberOfDeleted === 0) {
          logger.info("LaneRoutes::delete - 404 response sent back");
          return res.status(404).json("Not Found");
        }
        logger.info("LaneRoutes::delete - 204 response sent back");
        return res.status(204).json("No Content");
      })
      .catch(function (error) {
        return errorHandler.resolve(error, req, res);
      });
  } catch (error) {
    return errorHandler.resolve(error, req, res);
  }
};
