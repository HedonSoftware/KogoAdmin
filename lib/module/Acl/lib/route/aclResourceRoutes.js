
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath     = require("app-root-path");
var ResourceService = require("../service/resource");
var logger          = require(appRootPath + "/lib/logger/logger");

exports.get = function (req, res) {

};

exports.getById = function (req, res) {

};

exports.create = function (req, res) {

};

exports.update = function (req, res) {

};

exports.replace = function (req, res) {

};

exports.delete = function (req, res) {

};
