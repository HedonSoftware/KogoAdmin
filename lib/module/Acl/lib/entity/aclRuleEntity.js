
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var BaseEntity  = require(appRootPath + "/lib/entity/baseEntity");

class AclRuleEntity extends BaseEntity
{
  constructor(data)
  {
    super();

    this.id = undefined;
    this.userId = undefined;
    this.resourceId = undefined;
    this.permission = undefined;
    this.status = undefined;
    this.createdAt = undefined;
    this.updatedAt = undefined;

    if (data) {
      this.inflate(data);
    }
  }

  getId()
  {
    return this.id;
  }

  setId(id)
  {
    this.id = id;
    return this;
  }
}

module.exports = AclRuleEntity;
