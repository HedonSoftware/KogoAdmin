
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

var MysqlMapper    = require(appRootPath + "/lib/mapper/rethinkdb");
var logger            = require(appRootPath + "/lib/logger/logger");
var TicketActivity = require("./../entity/ticketActivity");
var Ticket         = require(appRootPath + "/lib/module/Ticket/lib/entity/ticket");
var User           = require(appRootPath + "/lib/module/User/lib/entity/user");
var util           = require("util");
var Exception      = require(appRootPath + "/lib/exception");
var _              = require(appRootPath + "/lib/utility/underscore");
var Request   = require(appRootPath + "/lib/request/request");

/**
 * TicketActivities mapper class definition
 * @param Connection dbConnection Optional DI for connection
 */
function TicketActivitiesMapper(dbConnection) {
  dbConnection = dbConnection || require(appRootPath + "/lib/db/mysql");
  this.setDbConnection(dbConnection);
};

/**
 * TicketActivitiesMapper extends MysqlMapper
 */
util.inherits(TicketActivitiesMapper, MysqlMapper);

/**
 * Table info
 * @type object
 */
TicketActivitiesMapper.prototype.table = {
  "name": "TicketActivity",
  "alias": "ta"
};

/**
 * Object describing ticketActivities"s relation
 *
 * @type Object
 */
TicketActivitiesMapper.prototype.relations = {
  "ticket": {
    "table": "Ticket",
    "localColumn": "ticketId",
    "targetColumn": "id",
    "defaultAlias": "t",
    "condition": "`ta`.`ticketId` = `t`.`id`",
    "query": "`Ticket` AS `t` ON `ta`.`ticketId` = `t`.`id`"
  },
  "user": {
    "table": "User",
    "localColumn": "userId",
    "targetColumn": "id",
    "defaultAlias": "u",
    "condition": "`ta`.`userId` = `u`.`id`",
    "query": "`User` AS `u` ON `ta`.`userId` = `u`.`id`"
  }
}

/**
 * Method fetches all records matching passed query builder"s criteria
 *
 * @param  Request request Used to specify the query
 * @return TicketActivitiesMapper this Fluent interface
 */
TicketActivitiesMapper.prototype.fetchAll = function (request) {

  var entities = [new TicketActivity()];
  var joins = request.getJoins();
  if (_.contains(joins, "ticket")) {
    entities.push(new Ticket());
  }
  if (_.contains(joins, "user")) {
    entities.push(new User());
  }

  var queryData = this.generateQuery(request, entities).toParam();
  return this.query(queryData.text, queryData.values);
};

TicketActivitiesMapper.prototype.save = function (ticketActivity) {

  if (!(ticketActivity instanceof TicketActivity)) {
    throw new Exception("Invalid entity passed. Instance of TicketActivity expected");
  }

  var me = this;
  var request = new Request();

  if (!ticketActivity.getId()) {
    return this.insert(ticketActivity);
  }

  request.setConditions({id: ticketActivity.getId()});
  return this.fetchOne(request)
    .then(function (ticketActivityFromDb) {

      if (!ticketActivityFromDb) {
        return me.insert(ticketActivity);
      }

      var baseTicketActivity = new TicketActivity(ticketActivityFromDb);

      // delete properties if not set
      if (!ticketActivity.createdAt) {
        delete ticketActivity.createdAt;
        ticketActivity.createdAt = baseTicketActivity.createdAt;
      }
      if (!ticketActivity.updatedAt) {
        delete ticketActivity.updatedAt;
      }

      return me.update(ticketActivity);
    });
};

TicketActivitiesMapper.prototype.insert = function (ticketActivity) {

  if (!(ticketActivity instanceof TicketActivity)) {
    throw new Exception("Invalid entity passed. Instance of TicketActivity expected");
  }

  var me = this;
  var queryData = this.generateInsertQuery(ticketActivity).toParam();
  return this.query(queryData.text, queryData.values)
    .then(function (data) {
      var details = data.shift();
      var request = new Request();
      request.setConditions({id: details.insertId});
      return me.fetchOne(request);
    })
    .then(function (ticketActivityData) {
      return new TicketActivity(ticketActivityData);
    });
};

TicketActivitiesMapper.prototype.update = function (ticketActivity) {

  if (!(ticketActivity instanceof TicketActivity)) {
    throw new Exception("Invalid entity passed. Instance of TicketActivity expected");
  }

  if (!ticketActivity.getId()) {
    throw new Exception("Invalid logic. Insert should be called");
  }

  var me = this;
  var queryData = this.generateUpdateQuery(ticketActivity).toParam();
  return this.query(queryData.text, queryData.values)
    .then(function (data) {
      var request = new Request();
      request.setConditions({id: ticketActivity.getId()});
      return me.fetchOne(request);
    })
    .then(function (ticketActivityData) {
      return new TicketActivity(ticketActivityData);
    });
};

TicketActivitiesMapper.prototype.delete = function (id) {

  var queryData = this.generateDeleteQuery({"id":id}).toParam();
  return this.query(queryData.text, queryData.values)
    .then(function (result) {
      var details = result.shift();
      return details.affectedRows;
    });
};

module.exports = TicketActivitiesMapper;
