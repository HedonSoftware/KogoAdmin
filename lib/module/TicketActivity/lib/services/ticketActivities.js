
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

var TicketActivityEntity = require("../entity/ticketActivity");
var Request        = require(appRootPath + "/lib/request/request");
var _                   = require(appRootPath + "/lib/utility/underscore");
var logger                 = require(appRootPath + "/lib/logger/logger");

/**
 * TicketActivities service class definition
 */
function TicketActivities(ticketActivitiesMapper) {
  if (!ticketActivitiesMapper) {
    var TicketActivitiesMapper = require("../mappers/ticketActivities");
    var ticketActivitiesMapper = new TicketActivitiesMapper;
  }

  this.ticketActivitiesMapper = ticketActivitiesMapper;
}

/**
 * Method gets all ticketActivities matching passed query
 * @param  {[type]}   query    [description]
 * @return {[type]}            [description]
 */
TicketActivities.prototype.all = function (query) {
  query = query || new Request();
  return this.ticketActivitiesMapper.fetchAll(query)
    .then(function (data) {
      return data.shift() || [];
    });
};

/**
 * Method gets single ticketActivity by passed id
 * @param  {[type]}   id       [description]
 * @return {[type]}            [description]
 */
TicketActivities.prototype.get = function (id) {
  var query = new Request();
  query.setConditions({"id": id});

  return this.all(query)
    .then(function (ticketActivities) {
      if (!_.isArray(ticketActivities) || _.isEmpty(ticketActivities)) {
        return null;
      }
      return ticketActivities.shift();
    });
};

TicketActivities.prototype.save = function (data) {
  var ticketActivity = new TicketActivityEntity(data);
  return this.ticketActivitiesMapper.save(ticketActivity);
};

TicketActivities.prototype.del = function (id) {
  return this.ticketActivitiesMapper.delete(id);
};

module.exports = TicketActivities;
