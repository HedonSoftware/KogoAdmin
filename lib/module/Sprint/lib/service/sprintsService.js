
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var SprintEntity = require("../entity/sprintEntity");
var Request      = require(appRootPath + "/lib/request/request");
var _            = require(appRootPath + "/lib/utility/underscore");

class SprintsService
{
  constructor(sprintGateway)
  {
    if (!sprintGateway) {
      var SprintGateway = require("../gateway/sprint/rethinkDbSprintGateway");
      sprintGateway = new SprintGateway();
    }

    this.sprintGateway = sprintGateway;
  }

  /**
   * Method gets all sprints matching passed query
   * @param  {[type]}   query    [description]
   * @return {[type]}            [description]
   */
  all(projectId, boardId, query)
  {
    query = query || new Request();
    return this.sprintGateway.fetchAll(projectId, boardId, query)
      .then(function (data) {
        return data || [];
      });
  }

  /**
   * Method gets single sprint by passed id
   * @param  {[type]}   id       [description]
   * @return {[type]}            [description]
   */
  get(projectId, boardId, query)
  {
    return this.all(projectId, boardId, query)
      .then(function (sprints) {
        if (!_.isArray(sprints) || _.isEmpty(sprints)) {
          return null;
        }
        return sprints.shift();
      });
  }

  create(projectId, boardId, data)
  {
    var sprint = new SprintEntity(data);
    return this.sprintGateway.insert(projectId, boardId, sprint);
  }

  update(projectId, boardId, data)
  {
    var sprint = new SprintEntity(data);
    return this.sprintGateway.update(projectId, boardId, sprint);
  }

  replace(projectId, boardId, data)
  {
    var sprint = new SprintEntity(data);
    return this.sprintGateway.replace(projectId, boardId, sprint);
  }

  delete(projectId, boardId, sprintId)
  {
    return this.sprintGateway.delete(projectId, boardId, sprintId);
  }
}

module.exports = SprintsService;
