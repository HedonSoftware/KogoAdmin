
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath    = require("app-root-path");
var SprintsService = require("../service/sprintsService");
var logger         = require(appRootPath + "/lib/logger/logger");
var errorHandler   = require(appRootPath + "/lib/error/errorHandler");

module.exports.get = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var sprintsService = new SprintsService();
  sprintsService.all(req.params.projectId, req.params.boardId, req.query)
    .then(function (sprints) {
      return res.status(200).json(sprints);
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.getById = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var queryConditions = req.query.getConditions();
  queryConditions.id = req.params.sprintId;
  req.query.setConditions(queryConditions);

  var sprintsService = new SprintsService();
  sprintsService.get(req.params.projectId, req.params.boardId, req.query)
    .then(function (sprint) {
      if (!sprint) return res.status(404).json("Not Found");
      return res.status(200).json(sprint);
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.create = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var sprintsService = new SprintsService();
  sprintsService.create(req.params.projectId, req.params.boardId, req.body)
    .then(function (sprint) {
      if (sprint === null) return res.status(404).json("Not Found");
      res.setHeader(
        "Location",
        "/projects/" + req.params.projectId + "/boards/" +
          req.params.boardId + "/sprints/" +  sprint.getId()
      );
      return res.status(201).json(sprint.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.update = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  req.body.id = req.params.sprintId;

  var sprintsService = new SprintsService();
  sprintsService.update(req.params.projectId, req.params.boardId, req.body)
    .then(function (sprint) {
      if (sprint === null) return res.status(409).json("Conflict");
      return res.status(200).json(sprint.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.replace = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  req.body.id = req.params.id;

  var sprintsService = new SprintsService();
  sprintsService.replace(req.params.projectId, req.params.boardId, req.body)
    .then(function (sprint) {
      if (sprint === null) return res.status(409).json("Conflict");
      return res.status(200).json(sprint.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.delete = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var sprintsService = new SprintsService();
  sprintsService.delete(req.params.projectId, req.params.boardId, req.params.sprintId)
    .then(function (numberOfDeleted) {
      if (numberOfDeleted === 0) return res.status(404).json("Not Found");
      return res.status(204).json("No Content");
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};
