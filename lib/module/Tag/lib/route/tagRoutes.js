
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var TagsService  = require("../service/tagsService");
var logger       = require(appRootPath + "/lib/logger/logger");
var errorHandler = require(appRootPath + "/lib/error/errorHandler");

module.exports.get = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var tagsService = new TagsService();
  tagsService.all(req.query)
    .then(function (tags) {
      return res.status(200).json(tags);
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.getById = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var queryConditions = req.query.getConditions();
  queryConditions.id = req.params.tagId;
  req.query.setConditions(queryConditions);

  var tagsService = new TagsService();
  tagsService.fetchOne(req.query)
    .then(function (tag) {
      if (!tag) return res.status(404).json("Not Found");
      return res.status(200).json(tag);
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.create = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var tagsService = new TagsService();
  tagsService.create(req.body)
    .then(function (tag) {
      if (tag === null) {
        return res.status(404).json("Not Found");
      }
      var locationHeader = "/tags/" +  tag.getId();
      res.setHeader("Location", locationHeader);
      return res.status(201).json(tag.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.update = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  req.body.id = req.params.tagId;

  var tagsService = new TagsService();
  tagsService.update(req.body)
    .then(function (tag) {
      if (tag === null) {
        return res.status(409).json("Conflict");
      }
      return res.status(200).json(tag.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.replace = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  req.body.id = req.params.id;

  var tagsService = new TagsService();
  tagsService.replace(req.body)
    .then(function (tag) {
      if (tag === null) {
        return res.status(409).json("Conflict");
      }
      return res.status(200).json(tag.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.delete = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var tagsService = new TagsService();
  tagsService.del(req.params.tagId)
    .then(function (numberOfDeleted) {
      if (numberOfDeleted === 0) {
        return res.status(404).json("Not Found");
      }
      return res.status(204).json("No Content");
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};
