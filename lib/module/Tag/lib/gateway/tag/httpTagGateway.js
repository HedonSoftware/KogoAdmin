
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var HttpGateway  = require(appRootPath + "/lib/gateway/httpGateway");
var TagEntity    = require(appRootPath + "/lib/module/Tag/lib/entity/tagEntity");
var GatewayError = require(appRootPath + "/lib/gateway/gatewayError");

class HttpTagGateway extends HttpGateway
{
  /**
   * Cutom contractor allows to pass data provider instance
   *
   * @param object httpDataProvider DataProvider's instance(i.e. httpDataProvider)
   */
  constructor(httpDataProvider)
  {
    super(httpDataProvider);
    this.dataProvider.apiUrl = "/tags";
  }

  save(tag)
  {
    return super.save(tag);
  }

  insert(tagEntity)
  {
    if (!(tagEntity instanceof TagEntity)) {
      throw new GatewayError("Invalid model passed. Instance of Tag expected");
    }

    return super.insert(tagEntity)
      .then(function(tagData) {
        return new TagEntity(tagData);
      });
  }

  update(tagEntity)
  {
    if (!(tagEntity instanceof TagEntity)) {
      throw new GatewayError("Invalid model passed. Instance of Tag expected");
    }

    if (!tagEntity.getId()) {
      throw new GatewayError("Invalid logic. Insert should be called");
    }

    return super.update(tagEntity)
      .then(function(tagData) {
        return new TagEntity(tagData);
      });
  }
}

module.exports = HttpTagGateway;
