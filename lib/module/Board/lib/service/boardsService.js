
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var BoardEntity  = require("../entity/boardEntity");
var Request      = require(appRootPath + "/lib/request/request");
var _            = require(appRootPath + "/lib/utility/underscore");
var ServiceError = require(appRootPath + "/lib/service/serviceError");
var logger       = require(appRootPath + "/lib/logger/logger");

class BoardsService
{
  constructor(boardGateway)
  {
    if (!boardGateway) {
      var BoardGateway = require("../gateway/board/httpBoardGateway");
      boardGateway = new BoardGateway();
    }

    this.boardGateway = boardGateway;
  }

  /**
   * Method gets all boards matching passed request
   * @param  {[type]}   request    [description]
   * @return {[type]}            [description]
   */
  all(projectId, request)
  {
    logger.info("BoardsService::all() method called", {
      projectId: projectId,
      request: request.export()
    });

    request = request || new Request();
    return this.boardGateway.fetchAll(projectId, request)
      .then(function (data) {
        return data || [];
      });
  }

  /**
   * Method gets single board by passed id
   * @param  projectId Project id
   * @return request   Request object
   */
  fetchOne(projectId, request)
  {
    logger.info("BoardsService::fetchOne() method called", {
      projectId: projectId,
      request: request
    });

    return this.all(projectId, request)
      .then(function (boards) {
        if (!_.isArray(boards) || _.isEmpty(boards)) {
          return null;
        }
        return boards.shift();
      });
  }

  create(projectId, data)
  {
    logger.info("BoardsService::create() method called", {
      projectId: projectId,
      data: data
    });

    var board = new BoardEntity(data);

    if (!_.isEmpty(board.lanes)) {
      throw new ServiceError(
        "Unable to insert board with lanes. " +
        "Use /projects/:projectId/board/:boardId:/lanes to insert lane",
        board,
        409
      );
    }

    return this.boardGateway.insert(projectId, board);
  }

  update(projectId, data)
  {
    logger.info("BoardsService::update() method called", {
      projectId: projectId,
      data: data
    });

    var board = new BoardEntity(data);

    if (!_.isEmpty(board.lanes)) {
      throw new ServiceError(
        "Unable to update board with lanes. " +
        "Use /projects/:projectId/boards/:boardId:/lanes to update lane",
        board,
        409
      );
    }

    return this.boardGateway.update(projectId, board);
  }
}

module.exports = BoardsService;
