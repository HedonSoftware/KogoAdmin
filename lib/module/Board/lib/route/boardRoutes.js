
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath   = require("app-root-path");
var BoardsService = require("../service/boardsService");
var logger        = require(appRootPath + "/lib/logger/logger");
var errorHandler  = require(appRootPath + "/lib/error/errorHandler");

module.exports.get = function (req, res) {
  logger.info("BoardRoutes::get - request received", {
    httpMethod: req.method,
    url: req.url,
    query: req.query.export()
  });

  var boardsService = new BoardsService();
  try {
    boardsService.all(req.params.projectId, req.query)
      .then(function (boards) {
        logger.info("BoardRoutes::get - 200 response sent back", {
          boards: boards
        });
        return res.status(200).json(boards);
      })
      .catch(function (error) {
        return errorHandler.resolve(error, req, res);
      });
  } catch (error) {
    return errorHandler.resolve(error, req, res);
  }
};

module.exports.getById = function (req, res) {
  logger.info("BoardRoutes::getById - request received", {
    httpMethod: req.method,
    url: req.url,
    query: req.query.export()
  });

  var queryConditions = req.query.getConditions();
  queryConditions.id = req.params.boardId;

  var boardsService = new BoardsService();
  try {
    boardsService.fetchOne(req.params.projectId, req.query)
      .then(function (board) {
        if (!board) {
          logger.info("BoardRoutes::getById - 404 response sent back");
          return res.status(404).json("Not Found");
        }
        logger.info("BoardRoutes::getById - 200 response sent back", {
          board: board
        });
        return res.status(200).json(board);
      })
      .catch(function (error) {
        return errorHandler.resolve(error, req, res);
      });
  } catch (error) {
    return errorHandler.resolve(error, req, res);
  }
};

module.exports.create = function (req, res) {
  logger.info("BoardRoutes::create - request received", {
    httpMethod: req.method,
    url: req.url,
    body: req.body,
    params: req.params
  });

  let projectId = req.params.projectId;
  let boardsService = new BoardsService();
  try {
    boardsService.create(projectId, req.body)
      .then(function (board) {
        if (board === null) {
          logger.info("BoardRoutes::create - 404 response sent back");
          return res.status(404).json("Not Found");
        }

        var locationHeader = "/projects/" + projectId + "/boards/" + board.getId();

        res.setHeader("Location", locationHeader);
        logger.info("BoardRoutes::create - 201 response sent back", {
          locationHeader: locationHeader,
          board: board
        });
        return res.status(201).json(board.export());
      })
      .catch(function (error) {
        return errorHandler.resolve(error, req, res);
      });
  } catch (error) {
    return errorHandler.resolve(error, req, res);
  }
};

module.exports.update = function (req, res) {
  logger.info("BoardRoutes::update - request received", {
    httpMethod: req.method,
    url: req.url,
    body: req.body,
    params: req.params
  });

  req.body.id = req.params.boardId;

  var boardsService = new BoardsService();
  try {
    boardsService.update(req.params.projectId, req.body)
      .then(function (board) {
        if (board === null) {
          logger.info("BoardRoutes::update - 409 response sent back");
          return res.status(409).json("Conflict");
        }
        logger.info("BoardRoutes::update - 200 response sent back", {
          board: board
        });
        return res.status(200).json(board.export());
      })
      .catch(function (error) {
        return errorHandler.resolve(error, req, res);
      });
  } catch (error) {
    return errorHandler.resolve(error, req, res);
  }
};
