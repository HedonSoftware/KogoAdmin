
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath      = require("app-root-path");
var TicketsService   = require("../service/ticketsService");
var logger           = require(appRootPath + "/lib/logger/logger");
var errorHandler = require(appRootPath + "/lib/error/errorHandler");

module.exports.get = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var ticketsService = new TicketsService();
  ticketsService.all(req.query)
    .then(function (tickets) {
      return res.status(200).json(tickets);
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.getById = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var queryConditions = req.query.getConditions();
  queryConditions.id = req.params.id;
  req.query.setConditions(queryConditions);

  var ticketsService = new TicketsService();
  ticketsService.get(req.query)
    .then(function (ticket) {
      if (!ticket) return res.status(404).json("Not Found");
      return res.status(200).json(ticket);
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.create = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var ticketsService = new TicketsService();
  ticketsService.create(req.body)
    .then(function (ticket) {
      if (ticket === null) return res.status(404).json("Not Found");
      res.setHeader("Location", "/tickets/" +  ticket.getId());
      return res.status(201).json(ticket.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.update = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  req.body.id = req.params.id;

  var ticketsService = new TicketsService();
  ticketsService.update(req.body)
    .then(function (ticket) {
      if (ticket === null) return res.status(409).json("Conflict");
      return res.status(200).json(ticket.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.replace = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  req.body.id = req.params.id;

  var ticketsService = new TicketsService();
  ticketsService.replace(req.body)
    .then(function (ticket) {
      if (ticket === null) return res.status(409).json("Conflict");
      return res.status(200).json(ticket.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.delete = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var ticketsService = new TicketsService();
  ticketsService.delete(req.params.id)
    .then(function (numberOfDeleted) {
      if (numberOfDeleted === 0) return res.status(404).json("Not Found");
      return res.status(204).json("No Content");
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};
