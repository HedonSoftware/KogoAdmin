
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath   = require("app-root-path");
var ProjectEntity = require("../entity/projectEntity");
var Request       = require(appRootPath + "/lib/request/request");
var _             = require(appRootPath + "/lib/utility/underscore");
var ServiceError  = require(appRootPath + "/lib/service/serviceError");

class ProjectsService
{
  constructor(projectGateway)
  {
    if (!projectGateway) {
      var ProjectGateway = require("../gateway/project/httpProjectGateway");
      projectGateway = new ProjectGateway();
    }

    this.projectGateway = projectGateway;
  }

  /**
   * Method gets all projects matching passed query
   * @param  {[type]}   query    [description]
   * @return {[type]}            [description]
   */
  all(query)
  {
    query = query || new Request();
    return this.projectGateway.fetchAll(query)
      .then(function (data) {
        return data || [];
      });
  }

  /**
   * Method gets single project by passed id
   * @param  {[type]}   id       [description]
   * @return {[type]}            [description]
   */
  get(query)
  {
    return this.all(query)
      .then(function (projects) {
        if (!_.isArray(projects) || _.isEmpty(projects)) {
          return null;
        }
        return projects.shift();
      });
  }

  create(data)
  {
    var project = new ProjectEntity(data);

    if (!_.isEmpty(project.boards)) {
      throw new ServiceError(
        "Unable to insert project with boards. " +
        "Use /projects/:projectId/boards to insert board",
        data,
        409
      );
    }

    return this.projectGateway.insert(project);
  }

  update(data)
  {
    var project = new ProjectEntity(data);

    if (!_.isEmpty(project.boards)) {
      throw new ServiceError(
        "Unable to update project with boards. " +
        "Use /projects/:projectId/boards/:boardId to update board",
        data,
        409
      );
    }

    return this.projectGateway.update(project);
  }

  replace(data)
  {
    var project = new ProjectEntity(data);
    return this.projectGateway.replace(project);
  }

  delete(id)
  {
    return this.projectGateway.delete(id);
  }
}

module.exports = ProjectsService;
