
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath   = require("app-root-path");
var HttpGateway   = require(appRootPath + "/lib/gateway/httpGateway");
var ProjectEntity = require(appRootPath + "/lib/module/Project/lib/entity/projectEntity");
var GatewayError  = require(appRootPath + "/lib/gateway/gatewayError");

class HttpProjectGateway extends HttpGateway
{
  /**
   * Cutom contractor allows to pass data provider instance
   *
   * @param object httpDataProvider DataProvider's instance(i.e. httpDataProvider)
   */
  constructor(httpDataProvider)
  {
    super(httpDataProvider);
    this.dataProvider.apiUrl = "/projects";
  }

  save(project)
  {
    return super.save(project);
  }

  insert(projectEntity)
  {
    if (!(projectEntity instanceof ProjectEntity)) {
      throw new GatewayError("Invalid model passed. Instance of Project expected");
    }

    return super.insert(projectEntity)
      .then(function(projectData) {
        return new ProjectEntity(projectData);
      });
  }

  update(projectEntity)
  {
    if (!(projectEntity instanceof ProjectEntity)) {
      throw new GatewayError("Invalid model passed. Instance of Project expected");
    }

    if (!projectEntity.getId()) {
      throw new GatewayError("Invalid logic. Insert should be called");
    }

    return super.update(projectEntity)
      .then(function(projectData) {
        return new ProjectEntity(projectData);
      });
  }
}

module.exports = HttpProjectGateway;
