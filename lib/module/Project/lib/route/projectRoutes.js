
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRoot         = require("app-root-path");
var ProjectsService = require("../service/projectsService");
var logger          = require(appRoot + "/lib/logger/logger");
var errorHandler    = require(appRoot + "/lib/error/errorHandler");

/**
 * Method returns json response for get multiple projects
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return String response Json response
 */
module.exports.get = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var projectsService = new ProjectsService();
  projectsService.all(req.query)
    .then(function (projects) {
      return res.status(200).json(projects);
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.getById = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var queryConditions = req.query.getConditions();
  queryConditions.id = req.params.id;
  req.query.setConditions(queryConditions);

  var projectsService = new ProjectsService();
  projectsService.get(req.query)
    .then(function (project) {
      if (!project) return res.status(404).json("Not Found");
      return res.status(200).json(project);
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.create = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var projectsService = new ProjectsService();
  projectsService.create(req.body)
    .then(function (project) {
      if (project === null) return res.status(404).json("Not Found");
      res.setHeader("Location", "/projects/" +  project.getId());
      return res.status(201).json(project.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

/**
 * Method updates project record
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
module.exports.update = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  req.body.id = req.params.projectId;
  delete req.params.projectId;

  var projectsService = new ProjectsService();
  projectsService.update(req.body)
    .then(function (project) {
      if (project === null) return res.status(409).json("Conflict");
      return res.status(200).json(project.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

/**
 * Method replaces project record
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
module.exports.replace = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  req.body.id = req.params.id;

  var projectsService = new ProjectsService();
  projectsService.replace(req.body)
    .then(function (project) {
      if (project === null) return res.status(409).json("Conflict");
      return res.status(200).json(project.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.delete = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var projectsService = new ProjectsService();
  projectsService.delete(req.params.id)
    .then(function (numberOfDeleted) {
      if (numberOfDeleted === 0) return res.status(404).json("Not Found");
      return res.status(204).json("No Content");
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};
