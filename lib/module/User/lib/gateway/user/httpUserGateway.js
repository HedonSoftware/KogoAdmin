
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath   = require("app-root-path");
var HttpGateway   = require(appRootPath + "/lib/gateway/httpGateway");
var UserEntity = require(appRootPath + "/lib/module/User/lib/entity/userEntity");
var _             = require(appRootPath + "/lib/utility/underscore");
var GatewayError  = require(appRootPath + "/lib/gateway/gatewayError");
var Request       = require(appRootPath + "/lib/request/request");

class HttpUserGateway extends HttpGateway
{
  /**
   * Cutom contractor allows to pass data provider instance
   *
   * @param object httpDataProvider DataProvider's instance(i.e. httpDataProvider)
   */
  constructor(httpDataProvider)
  {
    super(httpDataProvider);
    this.dataProvider.apiUrl = "/users";
  }

  save(user)
  {
    return super.save(user);
  }

  insert(userEntity)
  {
    if (!(userEntity instanceof UserEntity)) {
      throw new GatewayError("Invalid model passed. Instance of User expected");
    }

    return super.insert(userEntity)
      .then(function(userData) {
        return new UserEntity(userData);
      });
  }

  update(userEntity)
  {
    if (!(userEntity instanceof UserEntity)) {
      throw new GatewayError("Invalid model passed. Instance of User expected");
    }

    if (!userEntity.getId()) {
      throw new GatewayError("Invalid logic. Insert should be called");
    }

    return super.update(userEntity)
      .then(function(userData) {
        return new UserEntity(userData);
      });
  }
}

module.exports = HttpUserGateway;
