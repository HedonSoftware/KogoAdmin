
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var UsersService = require("../service/usersService");
var logger       = require(appRootPath + "/lib/logger/logger");
var errorHandler = require(appRootPath + "/lib/error/errorHandler");

module.exports.get = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var usersService = new UsersService();
  usersService.all(req.query)
    .then(function (users) {
      return res.status(200).json(users);
    }, function (error) {
      logger.error('Exception was thrown - ' + error.message, error);
      return errorHandler.resolve(error, req, res);
    })
    .catch(function (error) {
      logger.error('Error - ' + error);
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.getById = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var usersService = new UsersService();
  usersService.get(req.params.userId)
    .then(function (user) {
      if (!user) return res.status(404).json("Not Found");
      return res.status(200).json(user);
    }, function (error) {
      logger.error('Exception was thrown - ' + error.message, error);
      return errorHandler.resolve(error, req, res);
    })
    .catch(function (error) {
      logger.error('Error - ' + error);
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.create = function (req, res) {
  logger.info(req.method + " request: " + req.url, req.body);

  var usersService = new UsersService();
  usersService.save(req.body)
    .then(function (user) {
      if (user === null) return res.status(404).json("Not Found");
      res.setHeader("Location", "/api/users/" +  user.getId());
      return res.status(201).json(user);
    }, function (error) {
      logger.error('Exception was thrown - ' + error.message, error);
      return errorHandler.resolve(error, req, res);
    })
    .catch(function (error) {
      logger.error('Error - ' + error);
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.update = function (req, res) {
  logger.info(req.method + " request: " + req.url, req.body);

  req.body.id = parseInt(req.params.userId, 10);

  var usersService = new UsersService();
  usersService.save(req.body)
    .then(function (user) {
      if (user === null) return res.status(409).json("Conflict");
      return res.status(200).json(user);
    }, function (error) {
      logger.error('Exception was thrown - ' + error.message, error);
      return errorHandler.resolve(error, req, res);
    })
    .catch(function (error) {
      logger.error('Error - ' + error);
      return errorHandler.resolve(error, req, res);
    });
};
