
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

/**
 * This file will load all User service"s related files
 */

// export all routes
module.exports.Routes = {
  ApiRoutes: require("./lib/route/apiRoutes"),
  FrontendRoutes: require("./lib/route/frontendRoutes")
};

// export all services
module.exports.Services = {
  UsersService: require("./lib/service/usersService")
};

// export all gateways
module.exports.Gateways = {
  HttpUserGateway: require("./lib/gateway/user/httpUserGateway")
};

// export all entities
module.exports.Entities = {
  UserEntity: require("./lib/entity/userEntity")
};
