
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

class BaseGateway
{
  /**
   * Cutom contractor allows to inject instance of data provider
   *
   * @param object dataProvider Gateway's data provider(i.e. dbConnection)
   */
  constructor(dataProvider)
  {
    this.dataProvider = dataProvider;
  }

  /**
   * Setter for data provider instance
   *
   * @param object dataProvider Gateway's data provider(i.e. dbConnection)
   * @return self this Fluent interface
   */
  setDataProvider(dataProvider)
  {
    this.dataProvider = dataProvider;
    return this;
  }

  /**
   * Getter for data provider instance
   *
   * @return object dataProvider Gateway's data provider(i.e. dbConnection)
   */
  getDataProvider()
  {
    return this.dataProvider;
  }
}

module.exports = BaseGateway;
