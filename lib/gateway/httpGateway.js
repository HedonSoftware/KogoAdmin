
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var BaseGateway  = require("./baseGateway");
var BaseEntity   = require("../entity/baseEntity");
var util         = require("util");
var qs           = require("qs");
var _            = require(appRootPath + "/lib/utility/underscore");
var Exception    = require(appRootPath + "/lib/error/baseError");
var Request      = require(appRootPath + "/lib/request/request");
var GatewayError = require(appRootPath + "/lib/gateway/gatewayError");

class HttpGateway extends BaseGateway
{
  /**
   * Cutom contractor allows to pass data provider instance
   *
   * @param object httpDataProvider DataProvider's instance(i.e. httpDataProvider)
   */
  constructor(httpDataProvider)
  {
    if (!httpDataProvider) {
      var HttpDataProvider = require(appRootPath + "/lib/dataProvider/httpDataProvider");
      httpDataProvider = new HttpDataProvider();
    }

    super(httpDataProvider);
    this.dataProvider.apiUrl = "/http-gateway";
  }

  /**
   * Method fetches all records matching passed request criteria
   *
   * @param  Request request Used to specify the query
   * @return Promise promise Promise of data from DB
   */
  fetchAll(request)
  {
    return this.dataProvider.sendGetRequest(request);
  }

  /**
   * Generic save entity method
   *
   * @param Entity entity Entity object
   */
  save(entity)
  {
    if (!(entity instanceof BaseEntity)) {
      throw new GatewayError("Invalid entity passed. Instance of Entity expected");
    }

    if (!entity.getId()) {
      return this.insert(entity);
    }

    var me = this;
    var request = new Request();
    request.setConditions({id: entity.getId()});
    return this.fetchAll(request)
      .then(function (results) {

        if (!_.isEmpty(results)) {
          return me.update(entity);
        }

        return me.insert(entity);
      });
  }

  /**
   * Generic insert entity method
   *
   * @param Entity entity Entity to be inserted
   */
  insert(entity)
  {
    if (!(entity instanceof BaseEntity)) {
      throw new GatewayError("Invalid entity passed. Instance of Entity expected");
    }

    return this.dataProvider.sendPostRequest(entity);
  }

  /**
   * Generic update entity method
   *
   * @param Entity entity Entity to be updated
   */
  update(entity)
  {
    if (!(entity instanceof BaseEntity)) {
      throw new GatewayError("Invalid entity passed. Instance of Entity expected");
    }

    if (!entity.getId()) {
      throw new GatewayError("Invalid logic. Insert should be called");
    }

    return this.dataProvider.sendPatchRequest(entity);
  }
}

module.exports = HttpGateway;
