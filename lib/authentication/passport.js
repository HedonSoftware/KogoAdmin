
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath   = require("app-root-path");
var passport      = require("passport");
var LocalStrategy = require("passport-local").Strategy;
var UserService   = require(appRootPath + "/lib/module/User/lib/service/usersService");
var userService   = new UserService();
var sha1Crypt     = require("sha1");
var Request       = require(appRootPath + "/lib/request/request");
var logger        = require(appRootPath + "/lib/logger/logger");
var _             = require(appRootPath + "/lib/utility/underscore");
var config        = require(appRootPath + "/lib/config/config");

var adminRoleId = "6eeac08b-818b-480f-896f-8e4ea3f57267";

// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing.
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {

  // password needs to be removed!
  user.password = null;

  done(null, user);
});

// Use the LocalStrategy within Passport.
//   Strategies in passport require a `verify` function, which accept
//   credentials (in this case, a username and password), and invoke a callback
//   with a user object.  In the real world, this would query a database;
//   however, in this example we are using a baked-in set of users.
passport.use(new LocalStrategy(
  function(username, password, done) {
    // asynchronous verification, for effect...
    process.nextTick(function () {

      // Find the user by username.  If there is no user with the given
      // username, or the password is not correct, set the user to `false` to
      // indicate failure and set a flash message.  Otherwise, return the
      // authenticated `user`.
      var request = new Request();
      request.setConditions({username: username});
      userService.all(request)
        .then(function(users) {

          if (!users || _.isEmpty(users)) {
            logger.error("Users mapper returned invalid(non-array) or empty result", users);
            return done(null, false, { message: "Unknown user"});
          }

          var user = users.pop();

          if (user.roleId != adminRoleId) {
            return done(null, false, { message: "Insufficient privileges" });
          }

          var secret = config.get("authentication:secret");
          var passwordHash = sha1Crypt(password + secret);

          if (user.password != passwordHash) {
            logger.info("User " + username + " provided invalid password");
            return done(null, false, { message: "Invalid password" });
          }

          return done(null, user);
        }).done();
    });
  }
));

module.exports = passport;
