
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var BaseEntity   = require("../entity/baseEntity");
var util         = require("util");
var qs           = require("qs");
var _            = require(appRootPath + "/lib/utility/underscore");
var Exception    = require(appRootPath + "/lib/error/baseError");
var Request      = require(appRootPath + "/lib/request/request");
var BaseError    = require(appRootPath + "/lib/error/baseError");

class HttpDataProvider
{
  /**
   * Method returns HTTP client details
   */
  getHttpClient()
  {
    if (!this.httpClient) {
      var HTTP = require("q-io/http");
      var configuration = require("../config/config");
      HTTP.config = configuration.get("cerberus-api");
      this.httpClient = HTTP;
    }

    return this.httpClient;
  }

  /**
   * Method sends GET HTTP request
   */
  sendGetRequest(request)
  {
    if (!(request instanceof Request)) {
      throw new Exception("Invalid request passed. Instance of Request expected");
    }

    var HttpClient = this.getHttpClient();

    var queryString = qs.stringify(request.export(), { indices: false });

    var requestObject = _.extend({}, HttpClient.config);
    requestObject.path = this.apiUrl;
    requestObject.method = "GET";

    if (queryString) {
      requestObject.path += "?" + queryString;
    }

    return HttpClient.request(requestObject)
      .then(function (response){
        return response.body.read();
      }).then(function (buffer){
        return JSON.parse(buffer.toString());
      });
  }

  /**
   * Method sends POST HTTP request
   */
  sendPostRequest(entity)
  {
    if (!(entity instanceof BaseEntity)) {
      throw new Exception("Invalid model passed. Instance of BaseEntity expected");
    }

    var HttpClient = this.getHttpClient();

    var requestObject = _.extend({}, HttpClient.config);
    requestObject.path = this.apiUrl;
    requestObject.method = "POST";
    requestObject.body = [JSON.stringify(entity)];

    return HttpClient.request(requestObject)
      .then(function (response){
        return response.body.read();
      }).then(function (buffer){
        return JSON.parse(buffer.toString());
      });
  }

  /**
   * Method sends PUT HTTP request
   */
  sendPutRequest(entity)
  {
    if (!(entity instanceof BaseEntity)) {
      throw new Exception("Invalid model passed. Instance of BaseEntity expected");
    }

    var HttpClient = this.getHttpClient();

    var requestObject = _.extend({}, HttpClient.config);
    requestObject.path = this.apiUrl + "/" + entity.getId();
    requestObject.method = "PUT";
    requestObject.body = [JSON.stringify(entity)];

    return HttpClient.request(requestObject)
      .then(function (response){
        return response.body.read();
      }).then(function (buffer){
        return JSON.parse(buffer.toString());
      });
  }

  /**
   * Method sends PATCH HTTP request
   */
  sendPatchRequest(entity)
  {
    if (!(entity instanceof BaseEntity)) {
      throw new Exception("Invalid model passed. Instance of BaseEntity expected");
    }

    var HttpClient = this.getHttpClient();

    var requestObject = _.extend({}, HttpClient.config);
    requestObject.path = this.apiUrl + "/" + entity.getId();
    requestObject.method = "PATCH";
    requestObject.body = [JSON.stringify(entity)];

    return HttpClient.request(requestObject)
      .then(function (response){
        return response.body.read();
      }).then(function (buffer){
        return JSON.parse(buffer.toString());
      });
  }
}

module.exports = HttpDataProvider;
