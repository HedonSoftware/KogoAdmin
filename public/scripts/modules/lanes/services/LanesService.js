
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

var lanesService = angular.module('LanesService', []);

lanesService.factory('LanesService', ['$http', function($http) {

  "use strict";

  var lanesApiUrl = '/api/projects/:projectId/boards/:boardId/lanes';

  return {
    // call to get all boards
    get : function(projectId, boardId, params) {
      var apiUrl = this.getBaseUrl(projectId, boardId);
      var result = $http.get(
        apiUrl + (params ? '?' + $.param(params) : '')
      );

      // formatting data
      return result.then(function(response){
        var rawLanes = response.data;

        var lanes = {};
        angular.forEach(rawLanes, function(lane) {
          lanes[lane.id] = lane;
        });

        return [rawLanes, lanes];
      });
    },

    getById : function(projectId, boardId, laneId) {
      return this.get(projectId, boardId, {'conditions': {'l.id': laneId}})
        .then(function(lanes){
          return lanes[id];
        });
    },

    save : function(projectId, boardId, laneData) {
      if (laneData.id) {
        return this.update(projectId, boardId, laneData.id, laneData);
      }

      return this.create(projectId, boardId, laneData);
    },

    // call to POST and create a new lane
    create : function(projectId, boardId, laneData) {
      var apiUrl = this.getBaseUrl(projectId, boardId);
      return $http.post(apiUrl, laneData)
        .then(function(result) {
          return result.data;
        });
    },

    update: function(projectId, boardId, laneId, laneData) {
      var apiUrl = this.getBaseUrl(projectId, boardId);
      return $http.put(apiUrl + '/' + laneId, laneData)
        .then(function(result) {
          return result.data;
        });
    },

    getBaseUrl : function (projectId, boardId) {
      lanesApiUrl = lanesApiUrl.replace(":projectId", projectId);
      return lanesApiUrl.replace(":boardId", boardId);
    }
  };

}]);
