
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

var laneModalController = angular.module(
  'LaneModalController',
  [
    'LanesService',
    'ProjectsService',
    'TagsService'
  ]
);

laneModalController.controller(
  'LaneModalController',
  [
    "$scope", "$modalInstance", "LanesService", "ProjectsService", "TagsService", "lane",
    function ($scope, $modalInstance, LanesService, ProjectsService, TagsService, lane
    ) {

      "use strict";

      $scope.modalLane = _.extend({}, lane);

      // method called to create lane
      $scope.createLane = function (lane) {

        if (!_.isObject(lane)) {
          throw new Error('Invalid lane passed');
        }

        return LanesService.save(lane.projectId, lane.boardId, lane);
      };

      // method called to update lane
      $scope.updateLane = function (lane) {

        if (!_.isObject(lane)) {
          throw new Error('Invalid lane passed');
        }

        return LanesService.save(lane.projectId, lane.boardId, lane);
      };

      // method gets all projects
      ProjectsService.get()
        .then(function (projects) {
          $scope.modalProjects = projects;
      });

      // method gets all projects
      TagsService.get({
        type: "board",
        status: "active"
      }).then(function (tags) {
          $scope.modalTags = tags;
      });

      // ---------------------------------------------
      // ---------- MODAL RELEATED FUNCTIONS ---------
      // ---------------------------------------------

      /**
       * Function called when 'create lane' was clicked
       *
       * Role:
       * - validate form input
       *   * if error -> show error
       * - use service to save lane
       *   * if sucess -> close modal
       *   * else -> show error
       */
      $scope.create = function (lane) {

        // saving lane
        return $scope.createLane(lane)
          .then(function (lane) {
            $modalInstance.close(lane);
          }, function (error) {
            console.log(error);
          });
      };

      /**
       * Function called when 'update lane' was clicked
       *
       * Role:
       * - validate form input
       *   * if error -> show error
       * - use service to save lane
       *   * if sucess -> close modal
       *   * else -> show error
       */
      $scope.update = function (lane) {

        // saving lane
        return $scope.updateLane(lane)
          .then(function (lane) {
            $modalInstance.close(lane);
          }, function (error) {
            console.log(error);
          });
      };

      /**
       * Function called when 'delete' was clicked
       *
       * Role:
       * - validate form input
       *   * if error -> show error
       * - use service to save lane
       *   * if sucess -> close modal
       *   * else -> show error
       */
      $scope.delete = function (lane) {

        var data = {
          "id": lane.id,
          "status": "deleted",
          "projectId": lane.projectId,
          "boardId": lane.boardId,
        };

        // saving lane
        return $scope.updateLane(data)
          .then(function (lane) {
            $modalInstance.close(lane);
          }, function (error) {
            console.log(error);
          });
      };

      /**
       * Cancel method called when 'cancel' button
       * was clicked
       */
      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
    }
  ]
);
