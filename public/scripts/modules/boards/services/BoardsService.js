
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

var boardsService = angular.module('BoardsService', []);

boardsService.factory('BoardsService', ['$http', function ($http) {

  "use strict";

  var boardsApiUrl = '/api/projects/:projectId/boards';

  return {

    // call to get all boards
    get : function (projectId, params) {
      var apiUrl = this.getBaseUrl(projectId);
      var result = $http.get(
        apiUrl + (params ? '?' + $.param(params) : '')
      );

      // formatting data
      return result.then(function (response) {
        return response.data;
      });
    },

    getById : function (projectId, boardId) {
      return this.get(
        projectId,
        {
          conditions: {
            'b.id': boardId
          }
        })
        .then(function (boards) {
          return boards.pop();
        });
    },

    save : function (projectId, board) {
      if (board.id) {
        return this.update(projectId, board.id, board);
      }

      return this.create(projectId, board);
    },

    // call to POST and create a new board
    create : function (projectId, board) {
      var apiUrl = this.getBaseUrl(projectId);
      return $http.post(apiUrl, board)
        .then(function (result) {
          return result.data;
        });
    },

    update: function (projectId, boardId, board) {
      var apiUrl = this.getBaseUrl(projectId);
      return $http.put(apiUrl + '/' + boardId, board)
        .then(function (result) {
          return result.data;
        });
    },

    getBaseUrl : function (projectId) {
      return boardsApiUrl.replace(":projectId", projectId);
    }
  };

}]);
