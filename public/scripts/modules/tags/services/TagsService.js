
/**
 * KogoAdmin (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/KogoAdmin for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/KogoAdmin/blob/master/LICENSE.md Proprietary software
 */

var tagsService = angular.module('TagsService', []);

tagsService.factory('TagsService', ['$http', function($http) {

  "use strict";

  var tagsApiUrl = '/api/tags';

  return {
    // call to get all tags
    get : function(params) {
      var result = $http.get(
        tagsApiUrl + (params ? '?' + $.param(params) : '')
      );

      // formatting data
      return result.then(function(response){
        return response.data;
      });
    },

    getById : function(id) {
      return this.get(
          {
            conditions: {
              't.id': id
            }
          }
        )
        .then(function(tags){
          return tags.pop();
        });
    },

    save : function(tag) {
      if (tag.id) {
        return this.update(tag.id, tag);
      }

      return this.create(tag);
    },

    // call to POST and create a new tag
    create : function(tag) {
      return $http.post(tagsApiUrl, tag)
        .then(function(result) {
          return result.data;
        });
    },

    update: function(id, tag) {
      return $http.put(tagsApiUrl + '/' + id, tag)
        .then(function(result) {
          return result.data;
        });
    },

    // call to DELETE a tag
    delete : function(id) {
      return $http.delete(tagsApiUrl + '/' + id);
    }
  };

}]);
